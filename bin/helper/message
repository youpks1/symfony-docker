#!/usr/bin/env bash

    set -eE -o functrace

    # Colors
    red=31
    green=32
    yellow=33
    cyan=36
    bold=$(tput bold)

    echo_color() {
        # $1 is the color and $2 is the string to echo
        printf "\e[%sm%s\e[m\n" "$1" "$2"
    }

    echo_success() {
        echo_color ${green} "$1"
    }

    echo_info() {
        echo_color ${cyan} "$1"
    }

    echo_warn() {
        echo_color ${yellow} "$1"
    }

    echo_error() {
        >&2 echo_color ${red} "$1"
    }

    failure() {
      local lineno=$1
      local msg=$2
      echo "Failed at $lineno: $msg"
    }
    trap 'failure ${LINENO} "$BASH_COMMAND"' ERR