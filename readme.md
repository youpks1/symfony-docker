# Youpks Symfony Docker

---
## Information
> Some of the functions that are used are custom bash functions. 
You can find them on **[this](https://gitlab.com/youpks1/symfony-docker/-/wikis/Custom-Bash-scripts)** page.
Make sure you add them to your ```.bashrc``` or ```.zshrc``` file.

## Getting started

---
### Cloning the git repository
- Go to your projects directory.
- Run ```git clone https://gitlab.com/youpks1/symfony-docker.git {YOUR-PROJECT-NAME}```
- Run ```homer add {YOUR-PROJECT-NAME}.test```
- Run ```cd {YOUR-PROJECT-NAME}```

---
### Installing a project automatically

Run the folowing command in the wanted directory to install the project.

```console
./symfony-install
```

⚠️ When you receive this message:

```console
permission denied: ./symfony-install
```

You need to run this command to get it fixed:

```console
chmod 777 ./symfony-install
```

---
### Installing a project manually

- Run ```rm -rf .git > /dev/null```
- Run ```rm -rf app && mkdir app```
- Run ```sed -i '' "s/{{APPDOMAIN}}/$(get-domain)/g" docker-compose.yml``` or replace YOURDOMAIN with the local domain ```sed -i '' "s/{{APPDOMAIN}}/YOURDOMAIN/g" docker-compose.yml```
- Run ```docker-compose up -d --build --force-recreate --remove-orphans``` in the root of the project.
- Check if the environment is correctly set up with ```symfony check:requirements``` You should see this:

```console
[OK]
Your system is ready to run Symfony projects.
```

- Run ```symfony new .```.

```console
[OK] Your project is now ready in /var/www/symfony_docker
```

- Run ```rm -rf app/.git > /dev/null```
- Run ```docker-compose exec php composer req --dev maker ormfixtures fakerphp/faker```
- Run ```docker-compose exec php composer req doctrine twig```
- Run ```cp build/.env.example ./app/.env.local``` and fill in the empty fields.
- Run ```cp -f build/doctrine.yaml ./app/config/packages/doctrine.yaml```
- Run ```rm -rf "./app/docker-compose.yml" && rm -rf "./app/docker-compose.override.yml" && rm -rf "build"```
- Run ```git init -q && git add -A && git commit -q -m "Use Youpks Symfony for $(get-domain)"```
- Run ```docker-compose up -d``` or ```dcu -d```
- Run ```docker-compose exec php bash``` or ```dce php```


### You are ready to go!
